//
//  EWCZipItem.m
//  EWCompress
//
//  Created by Tzu-Yi Lin on 2013/12/18.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCZipItem.h"
#import "EWCZipFile.h"

#import "EWCZipDefine.h"

static NSLock *extractLock = nil;

@interface EWCZipFile (Private)
- (unzFile *)_referencedZipfile;
@end

@interface EWCZipItem () {
    NSString *_title;
    NSData *_rawData;
}

- (void)_extractData;

@end

@implementation EWCZipItem

@synthesize zipfile = _zipfile;
@synthesize title = _title, crypted = _crypted, filePosition = _filePosition;

+ (void)initialize
{
    extractLock = [[NSLock alloc] init];
}

- (id)initWithZipFile:(EWCZipFile *)zipfile fileInfo:(unz_file_info64)fileinfo
{
    self = [super init];

    if (self) {
        _zipfile = zipfile;
        _fileinfo = fileinfo;
    }

    return self;
}

- (BOOL)crypted
{
    return (_fileinfo.flag & 0x01) != 0;
}

- (size_t)uncompressedSize
{
    return _fileinfo.uncompressed_size;
}

- (size_t)compressedSize
{
    return _fileinfo.compressed_size;
}

- (NSData *)data
{
    if (!_rawData) {
        [self _extractData];
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(releaseData:) userInfo:nil repeats:NO];
    }

    return _rawData;
}

- (void)releaseData:(NSTimer *)timer
{
    _rawData = nil;
}

- (NSString *)description
{
    return [[super description] stringByAppendingFormat:@" filesize: %llu", _fileinfo.uncompressed_size];
}

#define EXT_BUFFER_SIZE     8192
- (void)_extractData
{
    NSMutableData *tempData = [[NSMutableData alloc] initWithCapacity:_fileinfo.uncompressed_size];

    [extractLock lock];

    long long int byteRead = 0;
    unzFile *file = [self.zipfile _referencedZipfile];

    unzGoToFilePos64(*file, &_filePosition);

    if (_fileinfo.uncompressed_size != 0) {
        int error = 0;
        error = unzOpenCurrentFilePassword(*file, NULL);

        if (error == UNZ_OK) {
            int bufferRead = 0;
            unsigned char buffer[EXT_BUFFER_SIZE];
            do {
                bufferRead = unzReadCurrentFile(*file, buffer, EXT_BUFFER_SIZE);
                if (bufferRead > 0) {
                    [tempData appendBytes:buffer length:bufferRead];
                    byteRead += bufferRead;
                }
                else
                    break;
            } while(1);
            unzCloseCurrentFile(*file);
        }
    }

    if (byteRead > 0) {
        [self willChangeValueForKey:@"data"];
        _rawData = [tempData copy];
        [self didChangeValueForKey:@"data"];
    }

    [extractLock unlock];
}

@end

//
//  EWCZipItem.m
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCZipFile.h"
#import "EWCZipItem.h"
#import "EWCZipDefine.h"

#include "unzip.h"

// hidden or system depend filenames
#define OSXMetaFolderPrefix     "__MACOSX"
#define OSXDS_Store             ".DS_Store"

BOOL _EWC_readCompressDataGlobalInformation(const unzFile *file, unz_global_info64 *const info)
{
    int error = unzGetGlobalInfo64(*file, info);
    return error == UNZ_OK;
}

// additional define
@interface EWCZipFile () {
    unzFile _zipFile;
    unz_global_info64 _fileInfo;

    NSString *_password;

    NSStringEncoding _passwordEncoding;
    NSMutableArray *_files; // array of EWCItem

    NSInteger _filteredFileCount;
}

- (const char *)rawPassword;

@end

// object imp
@implementation EWCZipFile

@synthesize password = _password,
            hideSystemFiles = _hideSystemFiles,
            filenameEncoding = _filenameEncoding;

+ (NSArray *)supportedFileTypes
{
    return @[@"zip"];
}

+ (NSString *)identifier
{
    return @"EWCompressZIP";
}

- (id)initWithFilename:(NSString *)filename
{
    self = [super initWithFilename:filename];
    if (self) {
        _zipFile = unzOpen64([filename cStringUsingEncoding:NSUTF8StringEncoding]);
        if (!_zipFile)
            return nil;
        else {
            if (!_EWC_readCompressDataGlobalInformation(&_zipFile, &_fileInfo)) {
                unzClose(_zipFile);
                return nil;
            }
        }

        _passwordEncoding = NSASCIIStringEncoding;
        _filenameEncoding = NSUTF8StringEncoding;
        _filteredFileCount = 0;
        _hideSystemFiles = YES;
    }
    return self;
}

- (id)initWithFilename:(NSString *)filename password:(NSString *)pwd usingEncoding:(NSStringEncoding)passwordEncoding
{
    self = [self initWithFilename:filename];
    if (self) {
        _passwordEncoding = passwordEncoding;
        _password = pwd;
    }

    return self;
}

- (void)dealloc
{
    unzClose(_zipFile);
}

// instance message
- (const char *)rawPassword
{
    return [_password cStringUsingEncoding:_passwordEncoding];
}

- (NSUInteger)count
{
    return _fileInfo.number_entry - _filteredFileCount;
}

// setter and getter

- (unzFile *)_referencedZipfile
{
    return &_zipFile;
}

// protocol implementation
- (NSArray *)compressedFilesList
{
    if (!_files) {
        _files = [[NSMutableArray alloc] initWithCapacity:_fileInfo.number_entry];

        for (int i = 0; i < _fileInfo.number_entry; i++) {
            unz_file_info64 fileinfo;
            unzGetCurrentFileInfo64(_zipFile, &fileinfo, NULL, 0, NULL, 0, NULL, 0);

            // get file name
            char *rawFilename = malloc(fileinfo.size_filename + 1);
            unzGetCurrentFileInfo64(_zipFile, &fileinfo, rawFilename, fileinfo.size_filename + 1, NULL, 0, NULL, 0);

            // filter os x meta prefix
            if (_hideSystemFiles) {
                if (strncmp(OSXMetaFolderPrefix, rawFilename, strlen(OSXMetaFolderPrefix)) == 0) {
                    free(rawFilename);
                    _filteredFileCount++;
                    unzGoToNextFile(_zipFile);
                    continue;
                }
            }

            unz64_file_pos filepos;
            unzGetFilePos64(_zipFile, &filepos);

            EWCZipItem *item = [[EWCZipItem alloc] initWithZipFile:self fileInfo:fileinfo];
            item.title = [NSString stringWithCString:rawFilename encoding:_filenameEncoding];
            item.filePosition = filepos;

            if (item) {
                [_files addObject:item];
            }

            unzGoToNextFile(_zipFile);
            free(rawFilename);
        }
        
        /// sort files
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
        [_files sortUsingDescriptors:@[sort]];
    }

    return [_files copy];
}

@end

//
//  EWCZipDefine.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 2013/12/19.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#ifndef EWCompress_EWCZipDefine_h
#define EWCompress_EWCZipDefine_h


#define EWC_ZIP_MAX_FILENAME_LENGTH     (256)

#endif

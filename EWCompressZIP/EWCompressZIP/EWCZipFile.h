//
//  EWCZipItem.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCFile.h"

@class EWCItem;

@interface EWCZipFile : EWCFile

@property (nonatomic, copy) NSString *password;

// hide system depended folder and files
@property (nonatomic, assign) BOOL hideSystemFiles;
// zip file name encoding
@property (nonatomic, assign) NSStringEncoding filenameEncoding;

// Initial
- (id)initWithFilename:(NSString *)filename;
- (id)initWithFilename:(NSString *)filename password:(NSString *)pwd usingEncoding:(NSStringEncoding)passwordEncoding;

// number of files contained
- (NSUInteger)count;

@end


//
//  EWCZipItem.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 2013/12/18.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCItem.h"
#import "unzip.h"

@class EWCZipFile;

@interface EWCZipItem : EWCItem {
    unz_file_info64 _fileinfo;
}

@property (nonatomic, readonly) BOOL crypted;
@property (nonatomic, readonly, weak) EWCZipFile *zipfile;
@property (nonatomic, assign) unz64_file_pos filePosition;

- (id)initWithZipFile:(EWCZipFile *)zipfile fileInfo:(unz_file_info64)fileinfo;


@end

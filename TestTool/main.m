//
//  main.m
//  TestTool
//
//  Created by Tzu-Yi Lin on 13/9/7.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "unzip.h"

unzFile unzip;

void testFileNum()
{
    unz_global_info64 *fileinfo = malloc(sizeof(unz_file_info));
    int error = unzGetGlobalInfo64(unzip, fileinfo);

    if (error == UNZ_OK)
        NSLog(@"Number of files: %lld, comment size: %ld", fileinfo->number_entry, fileinfo->size_comment);

    free(fileinfo);
}

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDesktopDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        path = [path stringByAppendingPathComponent:@"test.zip"];

        unzip = unzOpen64([path cStringUsingEncoding:NSUTF8StringEncoding]);
        if (unzip) testFileNum(); else printf("cannot run\n");
    }

    return 0;
}


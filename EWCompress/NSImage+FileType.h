//
//  NSImage+FileType.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (FileType)

+ (NSImage *)imageForFile:(NSString *)file;

@end

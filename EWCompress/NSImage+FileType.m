//
//  NSImage+FileType.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "NSImage+FileType.h"

@implementation NSImage (FileType)

+ (NSImage *)imageForFile:(NSString *)file
{
    NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFile:file];
    
    // get bigger image
    NSArray *imageReps = [icon representations];
    NSSize biggerSize = NSZeroSize;
    for (NSImageRep *imageRep in imageReps) {
        NSSize size = imageRep.size;
        if (size.width > biggerSize.width)
            biggerSize = size;
    }
    
    [icon setSize:biggerSize];
    
    return icon;
}

@end

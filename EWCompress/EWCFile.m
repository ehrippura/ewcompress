//
//  EWCItem.m
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCFile.h"
#import "NSImage+FileType.h"

@implementation EWCFile

@synthesize icon = _icon;

+ (NSArray *)supportedFileTypes
{
    return @[];
}

+ (NSString *)identifier
{
    return nil;
}

- (id)initWithFilename:(NSString *)filename
{
    self = [super init];
    if (self) {
        _filename = filename;
        NSFileManager *fm = [NSFileManager defaultManager];
        BOOL isDirectory;
        if ([fm fileExistsAtPath:_filename isDirectory:&isDirectory]) {
            if (!isDirectory) {
                _fileURL = [NSURL fileURLWithPath:_filename];
            } else {
                return nil;
            }
        } else {
            return nil;
        }
    }
    return self;
}

- (NSString *)filename
{
    return _filename;
}

// empty implementation
- (NSArray *)compressedFilesList
{
    return nil;
}

- (NSImage *)icon
{
    if (!_icon) {
        _icon = [NSImage imageForFile:_filename];
    }
    
    return _icon;
}

- (NSString *)displayName
{
    if (!_displayName) {
        return [[_filename lastPathComponent] stringByDeletingPathExtension];
    }
    
    return _displayName;
}

- (BOOL)isEqual:(id)object
{
    return [self.filename isEqualToString:[object filename]];
}

//
//- (NSData *)decompressDataForFileIndex:(EWCFileIndex *)index
//{
//    return nil;
//}

@end

//
//  EWCItem.m
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import "EWCItem.h"

@implementation EWCItem

- (BOOL)getBinary:(void *)bytes length:(uint64_t)length
{
    return NO;
}

- (NSData *)data
{
    return nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@> - %p - %@", NSStringFromClass([self class]), self, self.title];
}

@end

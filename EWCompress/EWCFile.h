//
//  EWCItem.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

//@class EWCFileIndex;
@class EWCItem;


@protocol EWCFile <NSObject>

@required
+ (NSArray *)supportedFileTypes;
+ (NSString *)identifier;

- (id)initWithFilename:(NSString *)filename;
- (NSString *)filename;
- (NSArray *)compressedFilesList; // return list of EWCItem

//- (NSData *)decompressDataForFileIndex:(EWCFileIndex *)index;

@end

@interface EWCFile : NSObject <EWCFile> {
    NSString *_filename;
    NSString *_displayName;
}

@property (nonatomic, readonly) NSURL *fileURL;
@property (nonatomic, readonly) NSString *filename;
@property (nonatomic, readonly) NSImage *icon;
@property (nonatomic, copy) NSString *displayName;

@end


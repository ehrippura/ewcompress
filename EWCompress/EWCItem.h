//
//  EWCItem.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EWCItem : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSDate *createDate;
@property (nonatomic, copy) NSDate *modifiedDate;

@property (nonatomic, readonly) size_t compressedSize;
@property (nonatomic, readonly) size_t uncompressedSize;

- (BOOL)getBinary:(void *)bytes length:(uint64_t)length;

- (NSData *)data;

@end


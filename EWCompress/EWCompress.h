//
//  EWCompress.h
//  EWCompress
//
//  Created by Tzu-Yi Lin on 13/9/5.
//  Copyright (c) 2013 Eternal Wind. All rights reserved.
//


#ifndef EW_COMPRESS
#define EWCOMPRESS
#endif

#import <EWCompress/EWCItem.h>
#import <EWCompress/EWCFile.h>

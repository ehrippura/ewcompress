//
//  NSString+WideChar.h
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (WideChar)

+ (NSString *)stringWithWideChar:(wchar_t *)wchar;

@end

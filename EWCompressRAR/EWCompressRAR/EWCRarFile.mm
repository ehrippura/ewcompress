//
//  EWCRarFile.m
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "EWCRarFile.h"
#import "EWCRarItem.h"

@interface EWCRarFile() {
//    Archive *_rarFile;
    HANDLE _rarFile;
    RAROpenArchiveDataEx *_archiveData;
    
    NSMutableArray *_files;
    BOOL _open;
}

@end

@implementation EWCRarFile

+ (NSArray *)supportedFileTypes
{
    return @[@"rar"];
}

+ (NSString *)identifier
{
    return @"EWCompressRAR";
}

- (id)initWithFilename:(NSString *)filename
{
    self = [super initWithFilename:filename];
    if (self) {
        if (![self openFileInMode:RAR_OM_LIST])
            return nil;
        
        [self closeFile];
    }

    return self;
}

- (HANDLE)rarHandle
{
    return _rarFile;
}

- (void)dealloc
{
    [self closeFile];
}

static int openCount = 0, closeCount = 0;

- (BOOL)openFileInMode:(uint)mode
{
    const char *arcName = [self.filename cStringUsingEncoding:NSUTF8StringEncoding];
    
    _archiveData = new RAROpenArchiveDataEx;
    _archiveData->ArcName = new char[[self.filename length] + 1];
    bzero(_archiveData->ArcName, [self.filename length] + 1);
    _archiveData->ArcNameW = NULL;
    strncpy(_archiveData->ArcName, arcName, [self.filename length]);
    _archiveData->OpenMode = mode;
    
    _rarFile = RAROpenArchiveEx(_archiveData);
    
    _open = (_rarFile != NULL) && (_archiveData->OpenResult == ERAR_SUCCESS);
    return _open;
}

- (void)closeFile
{
    if (_rarFile) {
        if (RARCloseArchive(_rarFile) == ERAR_SUCCESS) {
            _rarFile = NULL;
            
            delete [] _archiveData->ArcName;
            delete _archiveData;
            
            _archiveData = NULL;
            _open = NO;
        }
    }
}

- (BOOL)isOpened
{
    return _open;
}

- (NSArray *)compressedFilesList
{
    if (!_files) {
        if (![self openFileInMode:RAR_OM_LIST])
            return nil;
        
        _files = [[NSMutableArray alloc] init];
        
        RARHeaderDataEx header;
        while (RARReadHeaderEx(_rarFile, &header) == ERAR_SUCCESS) {
            EWCRarItem *item = [[EWCRarItem alloc] initWithRarFile:self HeaderInfo:&header];
            if (item)
                [_files addObject:item];
            
            RARProcessFile(_rarFile, RAR_SKIP, NULL, NULL);
        }
        
        /// sort files
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
        [_files sortUsingDescriptors:@[sort]];
        
        [self closeFile];
    }
    
    return _files;
}

@end

//
//  EWCRarItem.h
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "EWCItem.h"
#include "rar.hpp"

@class EWCRarFile;

@interface EWCRarItem : EWCItem

@property (nonatomic, readonly) size_t blockPosition;
@property (nonatomic, readonly, weak) EWCRarFile *rarFile;

- (id)initWithRarFile:(EWCRarFile *)rarfile HeaderInfo:(RARHeaderDataEx *)header;

@end

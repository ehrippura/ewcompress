//
//  EWCRarFile.h
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "EWCFile.h"
#include "rar.hpp"

class Archive;

@interface EWCRarFile : EWCFile

@property (nonatomic, readonly) BOOL secured;
@property (nonatomic, copy) NSString *password;

- (HANDLE)rarHandle;
- (BOOL)openFileInMode:(uint)mode;
- (void)closeFile;
- (BOOL)isOpened;

@end

//
//  NSString+WideChar.m
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "NSString+WideChar.h"

#include <wchar.h>

@implementation NSString (WideChar)

+ (NSString *)stringWithWideChar:(wchar_t *)wchar
{
    NSStringEncoding encoding = (sizeof(wchar_t) == 4) ? NSUTF32LittleEndianStringEncoding : NSUTF16StringEncoding;
    return [[NSString alloc] initWithBytes:wchar
                                    length:(sizeof(wchar_t) * wcslen(wchar))
                                  encoding:encoding];
}

@end

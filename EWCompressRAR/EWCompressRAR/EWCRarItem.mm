//
//  EWCRarItem.m
//  EWCompressRAR
//
//  Created by Tzu-Yi Lin on 2014/5/11.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "EWCRarItem.h"
#import "EWCRarFile.h"

#include "rar.hpp"

static NSLock *ewvrar_extractLock = nil;

@implementation EWCRarItem

@synthesize rarFile = _rarFile,
            blockPosition = _blockPosition;

@synthesize compressedSize = _compressedSize, uncompressedSize = _uncompressedSize;

+ (void)initialize
{
    ewvrar_extractLock = [[NSLock alloc] init];
}

- (id)initWithRarFile:(EWCRarFile *)rarfile HeaderInfo:(RARHeaderDataEx *)header
{
    self = [super init];
    if (self) {
        _rarFile = rarfile;
        
        wchar_t *filename = header->FileNameW;
        self.title = [NSString stringWithWideChar:filename];
        
        RarTime filetime;
        filetime.SetDos(header->FileTime);
        self.modifiedDate = [NSDate dateWithTimeIntervalSince1970:filetime.GetUnix()];
        
        _uncompressedSize = header->UnpSize;
        _compressedSize = header->PackSize;
    }
    return self;
}

int CALLBACK __extractCallback(UINT msg, LPARAM UserData, LPARAM P1, LPARAM P2)
{
    NSMutableData *mdata = (__bridge NSMutableData *)(void *)UserData;
    if (msg == UCM_PROCESSDATA) {
        [mdata appendBytes:(void *)P1 length:P2];
    }
    
    return ERAR_SUCCESS;
}

- (NSData *)data
{
    [ewvrar_extractLock lock]; // prevent file access from other thread
    [_rarFile openFileInMode:RAR_OM_EXTRACT];
    
    HANDLE fileHandle = [_rarFile rarHandle];
    RARHeaderDataEx header;
    int pCode = 0;
    NSMutableData *mdata = nil;
    
    while (RARReadHeaderEx(fileHandle, &header) == ERAR_SUCCESS) {
        NSString *filename = [NSString stringWithWideChar:header.FileNameW];
        if ([self.title isEqualToString:filename]) {
            mdata = [[NSMutableData alloc] init];
            RARSetCallback(fileHandle, __extractCallback, (long)(__bridge void *)mdata);
            pCode = RARProcessFileW(fileHandle, RAR_TEST, NULL, NULL);
            break;
        }
        
        else RARProcessFile(fileHandle, RAR_SKIP, NULL, NULL);
    }
    
    [_rarFile closeFile];
    [ewvrar_extractLock unlock];
    
    if (mdata && pCode == ERAR_SUCCESS) {
        return [mdata copy];
    }
    
    return nil;
}

@end
